#include "mainwindow.h"
#include <QSystemSemaphore>
#include <QSharedMemory>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    /* Защита от повторного запуска */
    bool isRunning;
    QSystemSemaphore semaphore("<unic id 3220>", 1);
    QSharedMemory sharedMemory("<unic id 3321>");

    semaphore.acquire();

    if(sharedMemory.attach())
    {
        isRunning = true;
    }
    else
    {
        sharedMemory.create(1);
        isRunning = false;
    }

    semaphore.release();

    if(isRunning)
    {
        return 1;
    }
    /*------------------------------*/

    MainWindow w;
    w.show();

    return a.exec();
}
