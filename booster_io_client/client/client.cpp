#include "client.h"

Client::Client(QObject *parent) : QObject(parent)
{
    m_host = "";
    m_port = 0;
}

Client::~Client()
{

}

QString Client::host() const
{
    return m_host;
}

void Client::setHost(const QString &host)
{
    m_host = host;
}

quint16 Client::port() const
{
    return m_port;
}

void Client::setPort(const quint16 &port)
{
    m_port = port;
}

void Client::connectToHost(QString host, quint16 port)
{
    if(socket->isOpen())
    {
        disconnect();
    }

    qInfo() << "Connecting to: " << host << " on port " << port;

    socket->connectToHost(host,port);
}

void Client::disconnect()
{
    socket->disconnectFromHost();
    socket->close();
}

void Client::initConnection()
{
    socket = new QTcpSocket(this);
    connect(socket,&QTcpSocket::connected,this,&Client::connected);
    connect(socket,&QTcpSocket::disconnected,this,&Client::disconnected);

    connect(socket,&QTcpSocket::stateChanged,this,&Client::stateChanged);
    connect(socket,&QTcpSocket::readyRead,this,&Client::readyRead);

    connect(socket,QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),this,&Client::error);

    connectToHost(m_host,m_port);

    //socket->waitForDisconnected();
}

void Client::error(QAbstractSocket::SocketError socketError)
{
    qInfo() << "Error:" << socketError << " " << socket->errorString();
}

void Client::stateChanged(QAbstractSocket::SocketState socketState)
{
    QMetaEnum metaEnum = QMetaEnum::fromType<QAbstractSocket::SocketState>();
    qInfo() << "State: " << metaEnum.valueToKey(socketState);
}

void Client::readyRead()
{
    qInfo() << "Data from: " << sender() << " bytes: " << socket->bytesAvailable() ;
    QByteArray data;
    data = socket->readAll();
    qInfo() << "Data: " << data;

    qDebug() << QString::number(data.toUInt(), 2);

    emit readed(data);
}

void Client::sendCommand()
{
    if(sender()->objectName()== "btn_on_0")
    {
        socket->write("R0");
    }
    else if(sender()->objectName()== "btn_on_1")
    {
        socket->write("R1");
    }
    else if(sender()->objectName()== "btn_on_2")
    {
        socket->write("R2");
    }
    else if(sender()->objectName()== "btn_on_3")
    {
        socket->write("R3");
    }
    else if(sender()->objectName()== "btn_on_4")
    {
        socket->write("R4");
    }
    else if(sender()->objectName()== "btn_on_5")
    {
        socket->write("R5");
    }
    else if(sender()->objectName()== "btn_on_6")
    {
        socket->write("R6");
    }
    else if(sender()->objectName()== "btn_on_7")
    {
        socket->write("R7");
    }
    else if(sender()->objectName()== "btn_on_8")
    {
        socket->write("R8");
    }
    else if(sender()->objectName()== "btn_on_9")
    {
        socket->write("R9");
    }
    else if(sender()->objectName()== "btn_on_sn")
    {
        socket->write("R10");
    }
    else if(sender()->objectName()== "btn_off_0")
    {
        socket->write("r0");
    }
    else if(sender()->objectName()== "btn_off_1")
    {
        socket->write("r1");
    }
    else if(sender()->objectName()== "btn_off_2")
    {
        socket->write("r2");
    }
    else if(sender()->objectName()== "btn_off_3")
    {
        socket->write("r3");
    }
    else if(sender()->objectName()== "btn_off_4")
    {
        socket->write("r4");
    }
    else if(sender()->objectName()== "btn_off_5")
    {
        socket->write("r5");
    }
    else if(sender()->objectName()== "btn_off_6")
    {
        socket->write("r6");
    }
    else if(sender()->objectName()== "btn_off_7")
    {
        socket->write("r7");
    }
    else if(sender()->objectName()== "btn_off_8")
    {
        socket->write("r8");
    }
    else if(sender()->objectName()== "btn_off_9")
    {
        socket->write("r9");
    }
    else if(sender()->objectName()== "btn_off_sn")
    {
        socket->write("r10");
    }
}
