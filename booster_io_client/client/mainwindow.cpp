#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initVariables();
    initGui();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initVariables()
{
    client = new Client();
    clientThread = new QThread();

    client->moveToThread(clientThread);
    clientThread->start();

    connect(ui->btnConnect, &QPushButton::clicked, this, &MainWindow::initConnection);
    connect(this, &MainWindow::readyToConnect, client, &Client::initConnection);
    connect(ui->btnDisconnect, &QPushButton::clicked, client, &Client::disconnect);
    connect(client, &Client::readed, this, &MainWindow::processReadedData);

    connect(ui->btn_on_0, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_1, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_2, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_3, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_4, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_5, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_6, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_7, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_8, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_9, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_on_sn, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_0, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_1, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_2, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_3, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_4, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_5, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_6, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_7, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_8, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_9, &QPushButton::pressed, client, &Client::sendCommand);
    connect(ui->btn_off_sn, &QPushButton::pressed, client, &Client::sendCommand);
}

void MainWindow::initGui()
{
    setHostLineEditValidation();
    setPortLineEditValidation();

    /*
    ui->groupBoxIOButtons_1->setDisabled(true);
    ui->groupBoxIOButtons_2->setDisabled(true);
    ui->groupBoxIOButtons_sn->setDisabled(true);
    */

    connect(ui->btn_on_0, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_1, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_2, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_3, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_4, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_5, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_6, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_7, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_8, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_9, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_on_sn, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_0, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_1, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_2, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_3, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_4, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_5, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_6, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_7, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_8, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_9, &QPushButton::clicked, this, &MainWindow::checkIOButtons);
    connect(ui->btn_off_sn, &QPushButton::clicked, this, &MainWindow::checkIOButtons);

    ui->btnDisconnect->setDisabled(true);

    connect(client, &Client::connected, this, &MainWindow::setConnectedGuiState);
    connect(client, &Client::disconnected, this, &MainWindow::setDisconnectedGuiState);
}

void MainWindow::setHostLineEditValidation()
{
    QRegExp regExp("^(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])$");
    QRegExpValidator* validator = new QRegExpValidator(regExp);
    ui->lineEditHost->setValidator(validator);
}

void MainWindow::setPortLineEditValidation()
{
    QRegExp regExp("([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$");
    QRegExpValidator* validator = new QRegExpValidator(regExp);
    ui->lineEditPort->setValidator(validator);
}

void MainWindow::checkIOButtons()
{
    if((sender() == ui->btn_on_0))
    {
        ui->indicator_0->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if((sender() == ui->btn_on_1))
    {
        ui->indicator_1->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_2)
    {
        ui->indicator_2->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_3)
    {
        ui->indicator_3->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_4)
    {
        ui->indicator_4->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_5)
    {
        ui->indicator_5->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_6)
    {
        ui->indicator_6->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_7)
    {
        ui->indicator_7->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_8)
    {
        ui->indicator_8->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_9)
    {
        ui->indicator_9->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_on_sn)
    {
        ui->indicator_sn->setStyleSheet("background-image:url(:/icons/indicator_on.png)");
    }
    else if(sender() == ui->btn_off_0)
    {
        ui->indicator_0->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_1)
    {
        ui->indicator_1->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_2)
    {
        ui->indicator_2->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_3)
    {
        ui->indicator_3->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_4)
    {
        ui->indicator_4->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_5)
    {
        ui->indicator_5->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_6)
    {
        ui->indicator_6->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_7)
    {
        ui->indicator_7->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_8)
    {
        ui->indicator_8->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_9)
    {
        ui->indicator_9->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
    else if(sender() == ui->btn_off_sn)
    {
        ui->indicator_sn->setStyleSheet("background-image:url(:/icons/indicator_off.png)");
    }
}

void MainWindow::initConnection()
{
    client->setHost(ui->lineEditHost->text());
    client->setPort(ui->lineEditPort->text().toInt());

    emit readyToConnect();
}

void MainWindow::setConnectedGuiState()
{
    ui->btnConnect->setDisabled(true);
    ui->btnDisconnect->setEnabled(true);
    ui->lineEditHost->setDisabled(true);
    ui->lineEditPort->setDisabled(true);

    ui->groupBoxIOButtons_1->setEnabled(true);
    ui->groupBoxIOButtons_2->setEnabled(true);
    ui->groupBoxIOButtons_sn->setEnabled(true);
}

void MainWindow::setDisconnectedGuiState()
{
    ui->btnConnect->setEnabled(true);
    ui->btnDisconnect->setDisabled(true);
    ui->lineEditHost->setEnabled(true);
    ui->lineEditPort->setEnabled(true);

    ui->groupBoxIOButtons_1->setDisabled(true);
    ui->groupBoxIOButtons_2->setDisabled(true);
    ui->groupBoxIOButtons_sn->setDisabled(true);
}

void MainWindow::processReadedData(QByteArray data)
{
    if(data == "")
    {

    }
}
