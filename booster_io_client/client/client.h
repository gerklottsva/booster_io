#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QMetaEnum>

#include <QDebug>

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client(QObject *parent = nullptr);
    ~Client();

    QString host() const;
    void setHost(const QString &host);

    quint16 port() const;
    void setPort(const quint16 &port);

signals:
    void connected();
    void disconnected();
    void readed(QByteArray data);

public slots:
    void connectToHost(QString host, quint16 port);
    void disconnect();
    void initConnection();
    void sendCommand();

private slots:
    void error(QAbstractSocket::SocketError socketError);
    void stateChanged(QAbstractSocket::SocketState socketState);
    void readyRead();

private:
    QTcpSocket* socket;
    QString m_host;
    quint16 m_port;
};

#endif // CLIENT_H
