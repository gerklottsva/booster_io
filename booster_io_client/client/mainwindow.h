#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QRegExpValidator>
#include "client.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Client* client;
    QThread* clientThread;

    void initVariables();
    void initGui();

    void setHostLineEditValidation();
    void setPortLineEditValidation();

private slots:
    void checkIOButtons();
    void initConnection();
    void setConnectedGuiState();
    void setDisconnectedGuiState();
    void processReadedData(QByteArray data);

signals:
    void readyToConnect();
    void btnOn1Pressed();
    void btnOn2Pressed();
    void btnOn3Pressed();
    void btnOn4Pressed();
    void btnOn5Pressed();
};
#endif // MAINWINDOW_H
